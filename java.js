window.onload = function () {

  var alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
        'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
        't', 'u', 'v', 'w', 'x', 'y', 'z'];
  var topic;         
  var wordtoguess;         
  var correctword ;              
  var guess;
  var space;             
  var guesses = [ ];      
  var lives = 5 ;            
  var counter = 0;       
  
function letterbuttons()
  {
    myButtons = document.getElementById('A-Z');
    letters = document.createElement('ul');

        for (var x = 0; x < alphabet.length; x++) 
          {
              letters.id = 'alphabet';
              list = document.createElement('li');
              list.id = 'letter';
              list.innerHTML = alphabet[x];
              check();
              myButtons.appendChild(letters);
              letters.appendChild(list);
          }
  } 

function result ()
  {
        dashWord = document.getElementById('hold');
        ulLetter = document.createElement('ul');

    for (var i = 0; i < correctword.length; i++)
       {
          ulLetter.setAttribute('id','my-word');
          guess = document.createElement('li');
          guess.setAttribute('id','hold');
              if (correctword[i] === "-")
              {
                guess.innerHTML = "-";
                space = 1;
              }else
              {
                guess.innerHTML = "_";
              }
      guesses.push(guess);
      dashWord.appendChild(ulLetter);
      ulLetter.appendChild(guess);
      }

  }

function winLoss() 
   {
        var showLives = document.getElementById("mylives");
        showLives.innerHTML = lives;
        var LOSS = document.getElementById('loss');
        var WIN = document.getElementById('win');
          if (lives < 1)
          {
             LOSS.innerHTML = "GAMEOVER";
             document.getElementById('hold').innerHTML = "";
             document.getElementById('showAnswer').innerHTML = correctword;
          }    
          if (lives <= 5  && counter === guesses.length)
              {
              WIN.innerHTML = "YOU WIN!";
              }
      
  }
    
function selectCategory()
    {
       switch(wordtoguess)
       {
        case topic[0]:
            {
              categoryName.innerHTML = "Countries";
              break;
            }
        case topic[1]:
            {
              categoryName.innerHTML = "Vegetables";
              break;
            }
        case topic[2]:
            {
              categoryName.innerHTML = "Animals";
              break;
            } 
        case topic[3]:
            {
              categoryName.innerHTML = "Fruits";
              break;
            } 
       }
    }
  

function check()
    {
       list.onclick = function ()
        {
          var guess = (this.innerHTML);
          this.onclick = null;
              for (var x = 0; x < correctword.length; x++)
                  {
                    if (correctword[x] === guess)
                        {
                          guesses[x].innerHTML = guess;
                          counter += 1;
                        } 
                  }
          var x = (correctword.indexOf(guess));
              if (x === -1) 
                  {
                    lives -= 1;
                    winLoss();
                  } else
                  {
                    winLoss();
                  }
        }
  }
  
function play() {
topic = [
        ["colombia", "malaysia", "maldives", "pakistan", "slovenia","finland","zimbabwe","bulgaria","ethiopia","scotland"],
        ["broccoli", "cucumber", "garbanzo", "eggplant", "scallion","asparagus"],
        ["seahorse", "tortoise", "mongoose", "chipmunk","elephant","flamingo","kangaroo"],
        ["mandarin", "honeydrew", "chestnut", "strawberry","tamarind","mulberry","watermelon"]
        ];
    wordtoguess = topic[Math.floor(Math.random() * topic.length)];
    correctword = wordtoguess[Math.floor(Math.random() * wordtoguess.length)];
    correctword = correctword.replace(/\s/g, "-");
    letterbuttons();
    result();
    winLoss();
    selectCategory();
  }

  play();
}


